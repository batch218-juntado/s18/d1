// console.log("Hello world");

/*function printInfo(){
	let nickname=prompt("Enter your nickname: ");
	console.log("Hi, " + nickname);
}

printInfo(); // invoke

console.log("-------------------");
*/


function printName(name){ // the (name) is what we call a parameter.
	console.log("My name is " + name);
}

printName("Juana"); // Juana is the argument
printName("John");
printName("Cena");

// Now we have a reusable function / reusable task but could have different output based on what value to process with the help of... Parameters and Arguments

// [SECTION] Parameters and Arguments

// Parameter
	// "firstName" is called a parameter
	// A "parameter" acts as a named variable/container that exists only inside a function
	// It is used to store information that is provided to a function when it is called/invoked

// Argument
	// "Juana", "John", and "Cena" are the information/data provided directly into the function is called "argument".
	// Values passed when invoking a function are called arguments.
	// These arguments are then stored as the parameter within the function.


let sampleVariable="Inday";

printName(sampleVariable);
// Variables can also be passed as an argument

// --------------------------------

console.log("-------------------");

function checkDivisibilityBy8(num){
	let remainder=num%8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibilityBy8=remainder === 0;
	console.log("Is "+num+" divisible by 8?");
	console.log(isDivisibilityBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);


// [SECTION] Function as Argument
	// Function parameters can also accept functions as arguments
	// Some complex functions use other functions to perform more complicated results.

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.");
	}

	function invokeFunction(argumentFunction){
		argumentFunction();
	}

	invokeFunction(argumentFunction);
	console.log(argumentFunction);

console.log("-------------------");

// -------------------------------------

//[SECTION] Using Multiple Parameters

	function createFullName(firstName, middleName, lastName){
		console.log("My full name is " + firstName + " " + middleName + " " + lastName);
	}

					// it's better to have the arrangement of the parameter the same as the arrangement of the argument
	createFullName("Christopher", "Katigbak", "Malinao");

	// Use variables as an argument
	let firstName="John";
	let middleName="Doe";
	let lastName="Smith";

	createFullName(firstName, middleName, lastName);

// -------------------------------------	

	function getDifferenceof8Minus4(numA, numB){
		console.log("Difference: " + (numA-numB));
	}

	getDifferenceof8Minus4(8,4);
	// getDifferenceof8Minus4(4,8); // This will result in a logical error


console.log("-------------------");

// [SECTION] Return Statement

	// The "return" statement allows us to output a value from a function to be passed to the line/block of code that is invoked/called
	function returnFullName(firstName, middleName, lastName){
		// return firstName + " " + middleName + " " + lastName;

		// This line of code will not be printed
		console.log("This is printed inside a function");

		let fullName=firstName + " " + middleName + " " + lastName;
		return fullName;
	}

	// We could also create a variable inside the function to contain the result and return the variable instead.
	let completeName=returnFullName("Paul", "Smith", "Jordan");
	console.log(completeName);

	console.log("I am " + completeName);



	function printPlayerInfo(userName, level, job);{
		console.log("Username: " + userName);
		console.log("Level: " + level);
		console.log("Job: " + job);
	}

	let user1=printPlayerInfo("Username88", "Senior", "Programmer");
	console.log(user1);